#!/usr/bin/env python3
import socket
import time
import Diffi_Hellman
import RSA
import RC4

def isRSA(strIn):
	if strIn[len(strIn)-4:len(strIn)+1]=="-rsa":
		return True
	return False

def isRC4(strIn):
	if strIn[len(strIn)-4:len(strIn)+1]=="-rc4":
		return True
	return False

class Client:
	def __init__(self, host,port,key_length):
		self.host = host
		self.port = port
		self.key_length=key_length
		self.key=0
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.connect((self.host, self.port))
		self.RSA_params=RSA.RSA(key_length)
		self.RC4_params=RC4.RC4(0)

	def sendMess(self,mess,is_send):
		buf=""
		if isRSA(mess):
			mess=mess[0:len(mess)-4]
			buf=self.RSA_params.encrypt(mess)+"-rsa"
		elif isRC4(mess):
			mess=mess[0:len(mess)-4]
			buf=self.RC4_params.encrypt(mess)+"-rc4"
		else:
			buf=mess
		if is_send==True:
			self.s.send(buf.encode('utf8'))
		return mess

	def getAnsw(self):
		mess = self.s.recv(1024)
		mess=mess.decode('utf8')
		if isRSA(mess):
			mess=mess[0:len(mess)-4]
			mess=self.RSA_params.decrypt(mess)
		elif isRC4(mess):
			mess=mess[0:len(mess)-4]
			mess=self.RC4_params.decrypt(mess)
		return mess

	def generate_Diffi_Hellman_key(self):
		self.key=Diffi_Hellman.Diffi_Hellman_Client(self.key_length,self)
		self.RC4_params=RC4.RC4(self.key)
		print("Your key: ",self.key)

	def generate_RSA_keys(self,buf):
		self.RSA_params=RSA.RSA(self.key_length)
		self.sendMess(buf,True)
		rez = self.s.recv(1024)
		self.RSA_params.get_friend_key_for_Client(self)
		print("My public RSA key: ", self.RSA_params.my_public_key.n," ; ",self.RSA_params.my_public_key.e)
		print("My private RSA key: ", self.RSA_params.my_private_key.n," ; ",self.RSA_params.my_private_key.d)
		print("Server public RSA key: ", self.RSA_params.friend_public_key.n," ; ",self.RSA_params.friend_public_key.e)

	def init_RC4(self):
		self.RC4_params=RC4.RC4(self.key)

	def listen(self):
		while True:
			buf = input()
			if buf[0:7]=="rsakeys":
				self.generate_RSA_keys(buf)
			else:
				buf=self.sendMess(buf,True)
				if buf=="dhkey":
					self.generate_Diffi_Hellman_key()
				elif buf == "exit":
					break
				else:
					result = self.getAnsw()
					print('Ответ сервера: ', result)
				

		self.s.close()


cl=Client("127.0.0.1",11000,128)
cl.generate_Diffi_Hellman_key()
cl.init_RC4()
cl.generate_RSA_keys("rsakeys")
cl.listen()