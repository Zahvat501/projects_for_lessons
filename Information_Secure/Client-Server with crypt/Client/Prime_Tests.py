import sys
import random
import math
import Evklid

def Yakobi(a,b):
	if Evklid.gcd(a,b)!=1:
		return 0
	r=1
	if a<0:
		a=-a
		if b%4==3:
			r=-r
	while True:
	    t=0
	    while a%2==0:
		    t+=1
		    a/=2
	    if t%2!=0:
		    if b%8==3 or b%8==5:
			    r=-r
	    if a%4==3 and b%4==3:
		    r=-r
	    c=a
	    a=b%c
	    b=c
	    if a==0:
	    	return r

def prime_2_257(n):
        return (n == 2 or n == 3 or n == 5 or n == 7 or n == 11 or n == 13 or n == 17 or n == 19 or n == 23 or n == 29 or n == 31 or n == 37 or n == 41 or n == 43 or n == 47 or n == 53 or n == 59 or n == 61 or n == 67 or n == 71 
            or n == 73 or n == 79 or n == 83 or n == 89 or n == 97 or n == 101 or n == 103 or n == 107 or n == 109 or n == 113 or n == 127 or n == 131 or n == 137 or n == 139 or n == 149 or n == 151 or n == 157 or n == 163 or n == 167 or n == 173 
            or n == 179 or n == 181 or n == 191 or n == 193 or n == 197 or n == 199 or n == 211 or n == 223 or n == 227 or n == 229 or n == 233 or n == 239 or n == 241 or n == 251 or n == 257)

def primed(n):
    a = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 
    73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 
    179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257 ]
    for i in a:  
        if n % i == 0:
            return False
    return True

def MillerRabinTest(n):
    s=0 
    x=0 
    d = n - 1
    while d%2==0:  
        d //=2 
        s+=1 
    logn = math.log10(n)
    i=0
    while i<=logn:
        a=random.randint(2, n - 2)
        x = Evklid.FastModPow(a, d, n)
        if x == 1 or x == n - 1:
            i+=1 
            continue
        j=0
        while j<s-1:
            x = (x * x) % n
            if x == 1: 
                return False
            elif x == n - 1: 
                break
            j+=1
        if x != n - 1: 
            return False
        i+=1
    return True

def SoloveyShtrassenTest(n):
	k=math.log10(n)
	i=0
	while i<=k:
		a=random.randint(2, n - 2)
		if Evklid.gcd(a,n)>1:
			return False
		if Evklid.FastModPow(a,(n-1)//2,n)!=Yakobi(a,n)%n:
			return False
		i+=1
	return True

def number_is_prime_Miller_Rabin(n):
    if n < 2: 
        return False
    elif prime_2_257(n)==True: 
        return True
    elif primed(n)==False: 
        return False
    else: 
        return MillerRabinTest(n)

def number_is_prime_Solovey_Shtrassen(n):
    if n < 2: 
        return False
    elif prime_2_257(n)==True: 
        return True
    elif primed(n)==False: 
        return False
    else: 
        return SoloveyShtrassenTest(n)

def is_prime_MR(n):
    rez=number_is_prime_Miller_Rabin(n)
    if rez==True:
        print('is prime')
    else:
        print('is not prime')

def is_prime_SS(n):
    rez=number_is_prime_Solovey_Shtrassen(n)
    if rez==True:
        print('is prime')
    else:
        print('is not prime')

def primes_from_diap_MR(a,b):
	rez=[]
	i=a
	while i<=b:
		if number_is_prime_Miller_Rabin(i)==True:
			rez.append(i)
		i+=1
	return rez	

def primes_from_diap_SS(a,b):
	rez=[]
	i=a
	while i<=b:
		if number_is_prime_Solovey_Shtrassen(i)==True:
			rez.append(i)
		i+=1
	return rez	

'''
a=10
b=20
p1=primes_from_diap_MR(a,b)
p2=primes_from_diap_SS(a,b)
i=0
print('MR-test')
while i<len(p1):
	print(p1[i])
	i+=1
print('SS-test')
i=0
while i<len(p2):
	print(p2[i])
	i+=1'''