import get_random
import Evklid
import Prime_Tests
import random
import Hash

def strToInts(strIn):
	i=0
	while strIn[i]!=' ':
		i+=1
	first=strIn[0:i+1]
	second=strIn[i+1:len(strIn)]
	return int(first),int(second)

def p_generator(length):
	p=0
	mult=get_random.get_random_len(length)
	while True:
		if Prime_Tests.number_is_prime_Miller_Rabin(mult)==True:
			break
		mult+=2
	R=get_random.get_random_otr(2,4*mult+2)-1
	while True:
		p=mult*R+1
		if Prime_Tests.number_is_prime_Miller_Rabin(p)==True:
			break
		R+=2
	return p

def q_generator(length):
	q=get_random.get_random_len(length)
	while True:
		if Prime_Tests.number_is_prime_Miller_Rabin(q)==True:
			break
		q+=2
	return q


class private_key:
	def __init__(self,p,q,e):
		self.n=p*q
		fi=(p-1)*(q-1)
		self.d=Evklid.Inverse(e,fi)

class public_key:
	def __init__(self,n,e):
		self.n=n
		self.e=e
		
	def create_key(self,p,q):
		self.n=p*q
		fi=(p-1)*(q-1)
		self.e=random.randint(2,fi-1)
		while True:
			if Evklid.gcd(self.e,fi)==1:
				break
			self.e+=1

class RSA:
	def __init__(self, key_size):
		self.p=p_generator(key_size)
		self.q=q_generator(key_size)
		self.my_public_key=public_key(0,0)
		self.my_public_key.create_key(self.p,self.q)
		self.my_private_key=private_key(self.p,self.q,self.my_public_key.e)
		self.friend_public_key=public_key(0,0)
	
	def get_friend_key_for_Client(self,cl):
		buf=str(self.my_public_key.n)+' '+str(self.my_public_key.e)
		cl.sendMess(buf,True)
		rez = cl.getAnsw()
		fn,fe=strToInts(rez)
		self.friend_public_key=public_key(fn,fe)

	def encrypt_num(self,message):
		return Evklid.FastModPow(message,self.friend_public_key.e,self.friend_public_key.n)

	def decrypt_num(self,message):
		return Evklid.FastModPow(message,self.my_private_key.d,self.my_private_key.n)

	def encrypt(self,s):
		rez = "" 
		blok=0 
		bb = False
		i=0
		while i<len(s):
			blok = (ord)(s[i])
			bplus = 0
			i+=1
			while True:
				if i >= len(s): 
					break
				bb = False
				cur = (ord)(s[i])
				bplus = blok * 10000 + cur
				if bplus < self.friend_public_key.n: 
					blok = bplus 
					i+=1 
				else:
					rez += str(self.encrypt_num(blok))
					rez += " "
					bb = True
					break
		if bb==False: 
			rez += str(self.encrypt_num(blok))
		return rez

	def decrypt(self,s):
		rez = ""
		i=0
		while i<len(s):
			blok = ""
			while i < len(s)  and s[i] != ' ':  
				blok += str(s[i])
				i+=1 
			blok = str(self.decrypt_num(int(blok)))
			ravn = len(blok) % 4
			if ravn!=0:
				if ravn == 1: 
					blok = "000" + blok
				elif ravn == 2: 
					blok = "00" + blok
				else: 
					blok = "0" + blok
			j=0
			while j<len(blok):
				cur = str(blok[j]) + str(blok[j + 1]) + str(blok[j + 2]) + str(blok[j + 3])
				rez += (chr)(int(cur))
				j += 4
			i+=1
		return rez

	def ECP(self,message):
		h=Hash.Hash(message)
		h1=int(h,16)
		return hex(self.decrypt_num(h1))

	def ECP_check(self,s,message):
		h1=Hash.Hash(message)
		#h2=hex(Evklid.FastModPow(int(s,16),self.my_public_key.e,self.my_public_key.n))
		h2=hex(self.encrypt_num(int(s,16)))
		return h1==h2

'''A=RSA(128)
B=RSA(128)
A.friend_public_key=B.my_public_key
B.friend_public_key=A.my_public_key
cr=A.crypt(12345)
print(cr)
print(B.decrypt(cr))'''
#print(int(random.getrandbits(1024)))

#print(bin(127))
#print(Evklid.Bin(127))

'''r=RSA(128)
mess="My message"
en=r.ECP(mess)
ch=r.ECP_check(en,mess)
#print(en)
print(ch)'''