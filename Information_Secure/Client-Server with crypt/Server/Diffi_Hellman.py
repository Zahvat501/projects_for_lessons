import socket
import random
import Evklid
import Prime_Tests
import get_random

def numsFromStr(mess):
	n=''
	q=''
	M=''
	i=0
	while mess[i]!=' ':
		n+=mess[i]
		i+=1
	i+=1
	while mess[i]!=' ':
		q+=mess[i]
		i+=1
	i+=1
	while i<len(mess):
		M+=mess[i]
		i+=1
	return int(n),int(q),int(M)

def get_prime(length):
	q=get_random.get_random_len(length)
	while True:
		if Prime_Tests.number_is_prime_Miller_Rabin(q)==True:
			break
		q+=2
	return q

def Diffi_Hellman_Server(sr):
	mess=sr.getAnsw()
	n,q,M=numsFromStr(mess)
	y=random.randint(1,n-1)
	K=str(Evklid.FastModPow(q,y,n))
	sr.sendControlMess(K)
	Cy=Evklid.FastModPow(M,y,n)
	return Cy