import sys
import random
import math
import time

def Evklid(a, b):
    list=[] 
    AdivB = a//b
    AmodB=a%b 
    xOld = 0 
    yOld = 1 
    x = 0 
    y = 1
    list.append(AdivB) 
    maxIndex = 1
    while AmodB != 0:
        a = b
        b = AmodB
        AdivB = a//b
        AmodB=a%b
        list.append(AdivB)
        maxIndex+=1
    i = maxIndex - 2
    while i>=0:
        x = yOld 
        y = xOld - yOld * list[i]
        xOld = x 
        yOld = y
        i-=1
    return b,x,y

def gcd(a, b):
    if b==0:
        return 1
    NOD,x,y=Evklid(a, b)
    return NOD

def Inverse(num, modulus):
    if num == 0: 
        return 0
    nod, x, Y=Evklid(modulus, num)
    if Y < 0: 
        return Y + modulus
    else: 
        return Y

def InverseStr(s):
    sOut = ""
    i = len(s) - 1
    while s[i] == '0': 
    	i-=1
    while i>=0: 
    	sOut += s[i]
    	i-=1
    return sOut

def Bin(num):
    '''mod=0
    eBin = ""
    while num != 1 and num != 0:
        mod = num%2
        num = num//2
        eBin += str(mod)
    if num == 1: 
    	eBin += str(num)
    rez=InverseStr(eBin)'''
    return str(bin(num))[3:]

def FastModPow(a, e, N):
    eBin = Bin(e)
    rez = 0 
    oldRez = a
    for i in eBin:
        if i == '1': 
            rez = (oldRez * oldRez * a) % N
        else: 
            rez = (oldRez * oldRez) % N
        oldRez = rez        
    return rez

'''a=45239653077357468362550797865453085893851084693
b=272648787539031897657084520226989766389
c=656924281
print(FastModPow(a,b,c))'''