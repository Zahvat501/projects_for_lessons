from hashlib import md5

def Hash(message):
    return md5(str(message).encode()).hexdigest()