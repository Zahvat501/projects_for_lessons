def swap(a,b):
    return b,a

def key_scheduling(key):
    length_of_key= len(key)
    S = list(range(256))
    j=0
    for i in range(256):
        j = (j + S[i] + key[i % length_of_key]) % 256
        S[i], S[j] = swap(S[i], S[j])
    return S

def pseudo_random(S):
    i=j=0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = swap(S[i], S[j])
        t=(S[i] + S[j])% 256
        K = S[t]
        yield K

class RC4:
	def __init__(self,key):
		self.key=[ord(c) for c in str(key)]

	def get_keystream(self):
		S = key_scheduling(self.key)
		keystream = pseudo_random(S)
		return keystream

	def decrypt(self, _code):
	    code=_code.split(" ")
	    code.pop()
	    keystream=self.get_keystream()
	    r = []
	    for c in code:
	        r.append(int(c) ^ next(keystream))
	    message = ""
	    for c in r:
	        message += chr(c)
	    return message

	def encrypt(self, message):
	    keystream=self.get_keystream()
	    code = []
	    for c in message:
	        code.append(ord(c) ^ next(keystream))
	    rez=""
	    for i in code:
	    	rez+=(str(i).strip()+" ")
	    rez.strip()
	    return rez

'''r=RC4(976245)
en=r.encrypt("Hello")
print(en)
de=r.decrypt(en)
print(de)'''