#!/usr/bin/env python3
import socket
import random
import Diffi_Hellman
import RSA
import RC4

def isRSA(strIn):
	if strIn[len(strIn)-4:len(strIn)+1]=="-rsa":
		return True
	return False

def isRC4(strIn):
	if strIn[len(strIn)-4:len(strIn)+1]=="-rc4":
		return True
	return False

class Server:
	def __init__(self,host,port,key_length):
		self.host = host
		self.port = port
		self.key_length=key_length
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.key=0
		self.RSA_params=RSA.RSA(key_length)
		self.RC4_params=RC4.RC4(0)
		self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.s.bind((self.host, self.port))
		self.s.listen(5)
		self.sock, self.addr = self.s.accept()
		self.crypt_mode="none"
		print("client connected with address " + self.addr[0])

	def generate_Diffi_Hellman_key(self):
		self.key=Diffi_Hellman.Diffi_Hellman_Server(self)
		self.RC4_params=RC4.RC4(self.key)
		print("Your key: ",self.key)

	def generate_RSA_keys(self):
		self.RSA_params=RSA.RSA(self.key_length)
		self.RSA_params.get_friend_key_for_Server(self)
		print("My public RSA key: ", self.RSA_params.my_public_key.n," ; ",self.RSA_params.my_public_key.e)
		print("My private RSA key: ", self.RSA_params.my_private_key.n," ; ",self.RSA_params.my_private_key.d)
		print("Client public RSA key: ", self.RSA_params.friend_public_key.n," ; ",self.RSA_params.friend_public_key.e)

	def sendAutoMess(self,mess):
		if self.crypt_mode=="rsa":
			buf=self.RSA_params.encrypt(mess)+"-rsa"
			self.sock.send(buf.encode('utf8'))
		elif self.crypt_mode=="rc4":
			buf=self.RC4_params.encrypt(mess)+"-rc4"
			self.sock.send(buf.encode('utf8'))
		else:
			self.sock.send(mess.encode('utf8'))

	def sendControlMess(self,mess):
		if isRSA(mess):
			mess=mess[0:len(mess)-4]
			buf=self.RSA_params.encrypt(mess)+"-rsa"
			self.sock.send(buf.encode('utf8'))
		if isRC4(mess):
			mess=mess[0:len(mess)-4]
			buf=self.RC4_params.encrypt(mess)+"-rc4"
			self.sock.send(buf.encode('utf8'))
		else:
			self.sock.send(mess.encode('utf8'))
		return mess

	def getAnsw(self):
		buf = self.sock.recv(1024)
		buf = buf.rstrip()
		buf=buf.decode('utf8')
		self.crypt_mode="none"
		if isRSA(buf)==True:
			self.crypt_mode="rsa"
			buf=buf[0:len(buf)-4]
			buf=self.RSA_params.decrypt(buf)
		elif isRC4(buf)==True:
			self.crypt_mode="rc4"
			buf=buf[0:len(buf)-4]
			buf=self.RC4_params.decrypt(buf)
		return buf

	def init_RC4(self):
		self.RC4_params=RC4.RC4(self.key)

	def listen(self):
		while True:
			buf=self.getAnsw()
			if buf == "exit":
				self.sock.send(b"bye")
				break
			elif buf == "dhkey":
				#self.sock.send(b"diffi-hellman key generation")
				self.generate_Diffi_Hellman_key()
			elif buf == "rsakeys":
				self.generate_RSA_keys()
			elif buf:
				self.sendAutoMess(buf)
				print(buf)
		self.sock.close()

se=Server("127.0.0.1",11000,128)
se.generate_Diffi_Hellman_key()
se.init_RC4()
se.listen()