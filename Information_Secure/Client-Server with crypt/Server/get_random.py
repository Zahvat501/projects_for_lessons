import random

def get_random_len(length):
	rez=int(random.getrandbits(length))
	if rez<0:
		rez*=-1
	if rez%2==0:
		return rez+1
	return rez

def get_random_otr(first,last):
	rez=random.randint(first,last)
	if rez%2==0:
		if rez==last:
			return rez-1
		return rez+1
	return rez