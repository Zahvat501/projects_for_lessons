﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LZW_Archivator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e) //archive text
        {
            if (textBox2.Text != "")
            {
                textBox3.Text = "";
                List<int> list = LZW.LZW_encode(textBox2.Text);
                for (int i = 0; i < list.Count-1; i++) textBox3.Text += (list[i].ToString() + " ");
                textBox3.Text += list[list.Count - 1].ToString();
                label7.Text = "Text is archivated.";
            }
        }

        public static List<int> Code_in_Str_to_List(string s)
        {
            List<int> list = new List<int>();
            string cur = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] != ' ') cur += s[i];
                else
                {
                    list.Add(Convert.ToInt32(cur));
                    cur = "";
                }
            }
            list.Add(Convert.ToInt32(cur));
            return list;
        }

        private void button4_Click(object sender, EventArgs e) //dearchive text
        {
            if (textBox3.Text != "")
            {
                textBox4.Text = "";
                List<string> list = LZW.LZW_decode(Code_in_Str_to_List(textBox3.Text));
                for (int i = 0; i < list.Count; i++) textBox4.Text += (list[i].ToString());
                label7.Text = "Text is dearchivated.";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Текстовые файлы|*.txt";
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            textBox1.Text=openFileDialog1.FileName;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Архивированные файлы|*.lzw";
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            textBox5.Text = openFileDialog1.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "") LZW.LZW_txt_file_encode(textBox1.Text);
            label7.Text = "File is archivated.";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox5.Text != "") LZW.LZW_file_decode_to_txt(textBox5.Text);
            label7.Text = "File is dearchivated.";
        }
    }
}
