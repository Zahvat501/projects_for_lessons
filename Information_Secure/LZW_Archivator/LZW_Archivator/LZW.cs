﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LZW_Archivator
{
    class LZW
    {
        public static void LZW_txt_file_encode(string path = "")
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);
            List<int> rez = LZW_encode(reader.ReadToEnd());
            reader.Close();
            file.Close();
            BinaryWriter writer = new BinaryWriter(File.Open(path + ".lzw", FileMode.Create));
            for (int i = 0; i < rez.Count; i++) writer.Write(rez[i]);
            writer.Close();
        }

        public static void LZW_file_decode_to_txt(string path = "")
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(file, Encoding.ASCII);
            int number;
            List<int> list = new List<int>();
            while (reader.PeekChar() > -1)
            {
                number = reader.ReadInt32();
                list.Add(number);
            }
            reader.Close();
            file.Close();
            List<string> s = LZW_decode(list);
            FileStream file_out = new FileStream(path + ".txt", FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(file_out);
            for (int i = 0; i < s.Count; i++)
                writer.Write(s[i]);
            writer.Close();
            file_out.Close();
        }

        private static Dictionary<string, int> get_dict()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            for (int i = 0; i <= 65535; i++) dict.Add(((char)i).ToString(), i);
            return dict;
        }

        private static Dictionary<int, string> get_dict_Rev()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            for (int i = 0; i <= 65535; i++) dict.Add(i, ((char)i).ToString());
            return dict;
        }

        public static List<int> LZW_encode(string text)
        {
            Dictionary<string, int> dict = get_dict();
            List<int> code = new List<int>();
            string p = "", pc = "";
            char c;
            for (int i = 0; i < text.Length; i++)
            {
                c = text[i];
                pc = p + c;
                if (dict.ContainsKey(pc)) p = pc;
                else
                {
                    code.Add(dict[p]);
                    dict.Add(pc, dict.Count + 1);
                    p = c.ToString();
                }
            }
            code.Add(dict[p]);
            return code;
        }

        public static List<string> LZW_decode(List<int> code)
        {
            Dictionary<int, string> dict = get_dict_Rev();
            string pw = "", buf = "";
            List<string> text = new List<string>();
            int cw;
            for (int i = 0; i < code.Count; i++)
            {
                cw = code[i];
                if (dict.ContainsKey(cw))
                {
                    buf = dict[cw];
                    text.Add(buf);
                    dict.Add(dict.Count, pw + buf[0]);
                    pw = buf;
                }
                else
                {
                    pw += pw[0];
                    text.Add(pw);
                    dict.Add(dict.Count, pw);
                }
            }
            return text;
        }
    }
}
