import sys
import math

def Eratosfen(n):
    n+=1
    a=[False,False]
    i=2
    while i<n: 
        a.append(True)
        i+=1
    i=2
    while i*i<=n:
        if a[i]==True:
            j=i*i
            while j<n:
                a[j] = False
                j+=i
        i+=1
    rez=[]
    i=2
    while i<n:
        if a[i]==True:
            rez.append(i)
        i+=1
    return rez

def EratosfenWithOgran(n, down):
    n+=1
    down-=1
    a=[False,False]
    i=2
    while i<n: 
        a.append(True)
        i+=1
    i=2
    while i*i<=n:
        if a[i]==True:
            j=i*i
            while j<n:
                a[j] = False
                j+=i
        i+=1
    rez=[]
    i=2
    while i<n:
        if a[i]==True and i>down:
            rez.append(i)
        i+=1
    return rez