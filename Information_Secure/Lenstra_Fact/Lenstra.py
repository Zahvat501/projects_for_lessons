import sys
import random
import math
import time
import Eratosfen_Class
import Evklid
import Point
from threading import Thread
import _thread

class FactThread(Thread):

    def __init__(self, n,B1):
        Thread.__init__(self)
        self.l=_thread.allocate_lock()
        self.n=n
        self.B1=B1
        self.a=0
        self.b=0

    def run(self):
    	self.a=Lenstra_Fact(self.n,self.B1)
    	self.b=self.n//self.a
    	self.l.acquire()

def multPointsToPrimesS1(p, P, B1, n):
    delitel = 0 
    ready = False
    size = len(p)
    Ps = []
    i=0
    while i<size:
        Ps.append(P)
        pp = p[i]
        while pp < B1:
            Ps[i] = Point.kratPoint(Ps[i], p[i])
            NOD = Evklid.gcd(n, Ps[i]._x)
            if NOD > 1 and NOD != n:  
            	ready = True 
            	delitel = NOD 
            	break 
            NOD = Evklid.gcd(n, Ps[i]._y)
            if NOD > 1 and NOD != n: 
            	ready = True 
            	delitel = NOD 
            	break
            pp *= pp
        if ready==True: 
        	break
        i+=1
    return Ps,ready,delitel

def multPointsToPrimesS2(p, P, n):
    delitel = 0 
    ready = False
    size = len(p)
    Ps = [] 
    d = []
    i=0
    while  i<size-1:
     	d.append(Point.kratPoint(P, p[i + 1] - p[i]))
     	i+=1
    Ps.append(Point.kratPoint(P, p[0]))
    i=1
    while i<size:
        Ps.append(Point.sumPoints(Ps[i - 1], d[i - 1]))
        NOD = Evklid.gcd(n, Ps[i]._x)
        if NOD > 1 and NOD != n:  
        	ready = True 
        	delitel = NOD 
        	break 
        NOD = Evklid.gcd(n, Ps[i]._y)
        if NOD > 1 and NOD != n:  
        	ready = True 
        	delitel = NOD 
        	break 
        i+=1
    return Ps,ready,delitel

def Lenstra_Fact(n, B1):
    while True:
        #print('first')
        #print(B1)
        x = 0 
        y = 0 
        a = 0 
        b = 0 
        g = n
        while g == n:
            x = random.randint(1, n - 1)
            y = random.randint(1, n - 1)
            a = random.randint(1, n - 1)
            b = (y * y - x * x * x - a * x) % n
            if b < 0: 
            	b += n
            g = Evklid.gcd(n, 4 * a * a * a + 27 * b * b)
        if 1 < g and g < n: 
        	return g
        P = Point.Point(x, y, n, a)
        ready = False 
        delitel = 0
        p = Eratosfen_Class.Eratosfen(B1)
        Ps,ready,delitel = multPointsToPrimesS1(p, P, B1, n)
        if ready: 
        	return delitel
        #Second stage
        #print('second')
        P = Ps[len(Ps) - 1]
        B2 = B1 * B1
        #print(B2)
        ready = False 
        delitel = 0
        Q = Eratosfen_Class.EratosfenWithOgran(B2, B1)
        qP,ready,delitel = multPointsToPrimesS2(Q, P, n)
        if ready==True: 
        	return delitel
        B1 *= B1

def Lenstra_Alg(n):
    B=10
    th=FactThread(n,B)
    th.start()
    while not th.l.locked():
        pass
    return th.a,th.b

a,b=Lenstra_Alg(143)
print(a)
print(b)