import sys
import random
import math
import time
import Evklid

class Point:
	def __init__(self,x,y,q,a):
		self._x=x
		self._y=y
		self._q=q
		self._a=a

	def printPoint(self):
		print('('+str(self._x)+';'+str(self._y)+')')

def sumPoints(p1, p2):
    q = p1._q
    x1 = p1._x 
    y1 = p1._y
    x2 = p2._x 
    y2 = p2._y
    lambda_up = y2 - y1
    lambda_down = Evklid.Inverse(x2 - x1, q)
    lambda_ = (lambda_up * lambda_down) % q
    if lambda_ < 0: 
    	lambda_ += q
    x3 = (lambda_ * lambda_ - x1 - x2) % q 
    y3 = (lambda_ * (x1 - x3) - y1) % q
    if x3 < 0: 
    	x3 += q
    if y3 < 0: 
    	y3 += q
    rezP=Point(x3, y3,q,p1._a)
    return rezP

def doublePoint(Q):
    q = Q._q 
    a=Q._a
    x1 = Q._x 
    y1 = Q._y
    lambda_up = 3 * x1 * x1 + a
    lambda_down = Evklid.Inverse(2 * y1, q)
    lambda_ = (lambda_up * lambda_down) % q
    if lambda_ < 0: 
    	lambda_ += q
    x3 = (lambda_ * lambda_ - 2 * x1) % q 
    y3 = (lambda_ * (x1 - x3) - y1) % q
    if x3 < 0: 
    	x3 += q
    if y3 < 0: 
    	y3 += q
    rezP=Point(x3, y3,q,a)
    return rezP 

def kratPoint(Q, k): #неверно!!!!!!!!!!!
    q = Q._q
    kBin = Evklid.Bin(k)
    rez = Point(0,0,Q._q,Q._a) 
    cur = Point(0,0,Q._q,Q._a)
    i=len(kBin)-1
    l=0
    while(i>=0):
        if kBin[l] == '1':
            step = 2**i
            cur = Q
            j=1
            while j<step:
            	cur = doublePoint(cur)
            	j*=2
            if l == 0: 
            	rez = cur
            else: 
            	rez = sumPoints(rez, cur)
        i-=1
        l+=1
    return rez

#print(Evklid.FastModPow(123,24,12))