﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace RSA
{
    class BigNumsGenerator
    {
        private static BigInteger BinToDec(string s)
        {
            BigInteger rezult = 0;
            BigInteger pow = 1; int i = s.Length - 1;
            for (; i >= 0; i--,pow*=2) if (s[i] == '1') rezult += pow; 
            return rezult; 
         }

        public static BigInteger getRandom(int length)
        {
            Random random = new Random();
            string s = "";
            s += "1";
            for (int i = 1; i < length-1; i++) s += random.Next(0, 2);
            s += "1";
            BigInteger rezult = BinToDec(s);
            if (rezult < 0) rezult *= -1;
            return rezult;
        }

        public static BigInteger getRandom(BigInteger first, BigInteger last) //отрезок
        {
            if (last != -1)
            {
                Random random = new Random();
                Int64 bb = random.Next();
                BigInteger dt = new BigInteger(bb);
                dt = first + dt % (last - first + 1);
                if (dt < 0) dt *= -1;
                if ((dt & 1) == 1) return dt; //проверка на нечетность
                else
                {
                    if (dt == last) return dt - 1;
                    else return dt + 1;
                }
            }
            else
            {
                Random random = new Random();
                int length = first.ToString().Length+1;
                return getRandom(length);
            }
        }
    }
}
