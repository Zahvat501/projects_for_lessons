﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Windows.Forms;

namespace RSA
{
    class EvklidAndFastExp
    {
        public static void Evklid(BigInteger a, BigInteger b, out BigInteger NOD, out BigInteger X, out BigInteger Y, bool graphics, DataGridView datagridview=null)
        {
            List<BigInteger> list = new List<BigInteger>();
            BigInteger AmodB = 0, AdivB = BigInteger.DivRem(a, b, out AmodB), xOld = 0, yOld = 1, x = 0, y = 1;
            list.Add(AdivB); int maxIndex = 1;
            if (graphics) { datagridview[2, 0].Value = AmodB; datagridview[3, 0].Value = AdivB; }
            while (AmodB != 0)
            {
                a = b;
                b = AmodB;
                AdivB = BigInteger.DivRem(a, b, out AmodB);
                list.Add(AdivB);
                if (graphics) { datagridview.Rows.Add(); datagridview[0, maxIndex].Value = a; datagridview[1, maxIndex].Value = b; datagridview[2, maxIndex].Value = AmodB; datagridview[3, maxIndex].Value = AdivB; }
                maxIndex++;
            }
            if (graphics) { datagridview[4, maxIndex - 1].Value = x; datagridview[5, maxIndex - 1].Value = y; }
            for (int i = maxIndex - 2; i >= 0; i--)
            {
                x = yOld; y = xOld - yOld * list[i];
                xOld = x; yOld = y;
                if (graphics) { datagridview[4, i].Value = x; datagridview[5, i].Value = y; }
            }
            NOD = b; X = x; Y = y;
        }

        private static string InverseStr(String s)
        {
            String sOut = "";
            int i = s.Length - 1;
            while (s[i] == '0') i--;
            for (; i >= 0; i--) sOut += s[i];
            return sOut;
        }

        public static string Bin(BigInteger num)
        {
            BigInteger mod;
            string eBin = "";
            while (num != 1 && num != 0)
            {
                num = BigInteger.DivRem(num, 2, out mod);
                eBin += mod.ToString();
            }
            if (num == 1) eBin += num.ToString();
            return InverseStr(eBin);
        }

        public static BigInteger FastModPow(BigInteger a, BigInteger e, BigInteger N)
        {
            string eBin = Bin(e);
            BigInteger rez = 0, oldRez = a;
            for (int i = 1; i < eBin.Length; i++)
            {
                if (eBin[i] == '1') rez = (oldRez * oldRez * a) % N;
                else rez = (oldRez * oldRez) % N;
                oldRez = rez;
            }
            return rez;
        }
    }
}
