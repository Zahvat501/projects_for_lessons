﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Numerics;

namespace RSA
{
    public partial class Form1 : Form
    {
        private BigInteger _p, _q, _e, _d, _fi, _n;
        private static int _key_size;
        private string _text;

        private void Console_out(string s)
        {
            textBox13.SelectionStart = textBox13.Text.Length;
            textBox13.SelectedText += (">> " + s + Environment.NewLine);
            textBox13.Update();
        }

        public BigInteger Max(BigInteger a, BigInteger b)
        {
            if (a > b) return a;
            else return b;
        }

        private void generate_e()
        {
            Console_out("Generate e");
            _e = BigNumsGenerator.getRandom(2, _fi - 1);
            BigInteger nod, x, y;
            while (true)
            {
                EvklidAndFastExp.Evklid(_e, _fi, out nod, out x, out y, false);
                if (nod != 1) _e++;
                else break;
            }
        }

        private void generate_p()
        {
            Console_out("Generate mult for p-1");
            BigInteger mult = BigNumsGenerator.getRandom(_key_size);
            while (true)
            {
                if (Prime_Tests.number_is_prime(mult)) break;
                mult += 2;
            }
            Console_out("Generate p");
            BigInteger R = BigNumsGenerator.getRandom(2, 4 * mult + 2) - 1;
            while (true)
            {
                _p = mult * R + 1;
                if (Prime_Tests.number_is_prime(_p)) break;
                R += 2;
            }
        }

        private void generate_q()
        {
            _q = BigNumsGenerator.getRandom(_key_size);
            while (true)
            {
                if (Prime_Tests.number_is_prime(_q)) break;
                else _q += 2;
            }
        }

        private void generate_parametres()
        {
            generate_p();
            Console_out("Generate q");
            generate_q();
            Console_out("Calc n");
            _n = _p * _q;
            Console_out("Calc fi");
            _fi = (_p - 1) * (_q - 1);
            generate_e();
            Console_out("Calc d");
            /*BigInteger nod, x;
            EvklidAndFastExp.Evklid(_e, _fi, out nod, out x, out _d, false);
            if (_d < 0) _d += _fi;*/
            BigInteger nod, y;
            EvklidAndFastExp.Evklid(_e, _fi, out nod, out _d, out y, false);
            _d += Max(_e, _fi);
        }

        private bool form_is_correct_for_auto() //form fields is not empty 
        {
            return (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" &&
                textBox5.Text != "" && textBox6.Text != "" && textBox7.Text != "" && textBox8.Text != "");
        }

        private bool form_is_correct()
        {
            return (textBox1.Text != "" && textBox2.Text != "" && textBox5.Text != "");
        }

        private void vars_is_correct()
        {
            Console_out("Try parse p");
            _p = BigInteger.Parse(textBox1.Text);
            Console_out("Try parse q");
            _q = BigInteger.Parse(textBox2.Text);
            Console_out("Calc fi");
            _fi = (_p - 1) * (_q - 1);
            Console_out("Calc n");
            _n = _p * _q;
            Console_out("Try parse e");
            _e = BigInteger.Parse(textBox5.Text);
            Console_out("Calc d");
            /*BigInteger nod, x;
            EvklidAndFastExp.Evklid(_e, _fi, out nod, out x, out _d, false);
            if (_d < 0) _d += _fi;*/
            BigInteger nod, y;
            EvklidAndFastExp.Evklid(_e, _fi, out nod, out _d, out y, false);
            _d += Max(_e, _fi);
            textBox4.Text = Convert.ToString(_fi);
            textBox8.Text = Convert.ToString(_n);
            textBox6.Text = Convert.ToString(_d);
        }

        private string strToint(string s)
        {
            string rez = ""; BigInteger blok=0; bool bb = false;
            for (int i = 0; i < s.Length;)
            {
                blok = (int)s[i]; BigInteger bplus = 0;
                i++;
                while (true)
                {
                    if (i >= s.Length) break;
                    bb = false;
                    int cur = (int)s[i];
                    bplus = blok * 10000 + cur;
                    if (bplus < _n) { blok = bplus; i++; }
                    else
                    {
                        rez += EvklidAndFastExp.FastModPow(blok, _e, _n).ToString();
                        rez += " ";
                        bb = true;
                        break;
                    }
                }
            }
            if(!bb) rez += EvklidAndFastExp.FastModPow(blok, _e, _n).ToString();
            return rez;
        }

        private string intTostr(string s)
        {
            string rez = "";
            for (int i = 0; i < s.Length; i++)
            {
                string blok = "";
                while (i < s.Length  && s[i] != ' ') { blok += s[i].ToString(); i++; }
                blok = EvklidAndFastExp.FastModPow(BigInteger.Parse(blok), _d, _n).ToString();
                int ravn = blok.Length % 4;
                if (ravn!=0)
                {
                    if (ravn == 1) blok = "000" + blok;
                    else if (ravn == 2) blok = "00" + blok;
                    else blok = "0" + blok;
                }
                for (int j = 0; j < blok.Length; j += 4)
                {
                    string cur = blok[j].ToString() + blok[j + 1].ToString() + blok[j + 2].ToString() + blok[j + 3].ToString();
                    rez += (char)(Convert.ToInt32(cur));
                }
            }
            return rez;
        }

        private void button1_Click(object sender, EventArgs e) //generate parametres 
        {
            if (textBox3.Text != "")
            {
                Console_out("Try parse key size");
                _key_size = Convert.ToInt32(textBox3.Text);
                generate_parametres();
                textBox1.Text = Convert.ToString(_p);
                textBox2.Text = Convert.ToString(_q);
                textBox4.Text = Convert.ToString(_fi);
                textBox5.Text = Convert.ToString(_e);
                textBox6.Text = Convert.ToString(_d);
                textBox8.Text = Convert.ToString(_n);
            }
        }

        private void button2_Click(object sender, EventArgs e) //encrypt 
        {
            if (radioButton1.Checked)
            {
                Console_out("Encrypt function NOT auto");
                if (form_is_correct())
                {
                    Console_out("Form is correct");
                    vars_is_correct();
                    _text = textBox7.Text;
                    Console_out("Coding");
                    textBox7.Text = strToint(textBox7.Text);
                    Console_out("Coding is ready");
                }
                else Console_out("Form is not correct");
            }
            else
            {
                Console_out("Encrypt function is automatic");
                if (form_is_correct_for_auto())
                {
                    Console_out("Form is correct");
                    _text = textBox7.Text;
                    Console_out("Coding");
                    textBox7.Text = strToint(textBox7.Text);
                    Console_out("Coding is ready");
                }
                else Console_out("Form is not correct");
            }
        }

        private void button3_Click(object sender, EventArgs e) //decrypt 
        {
            if (radioButton1.Checked)
            {
                Console_out("Decrypt function NOT auto");
                if (form_is_correct())
                {
                    Console_out("Form is correct");
                    vars_is_correct();
                    _text = textBox7.Text;
                    Console_out("Decoding");
                    textBox7.Text = intTostr(textBox7.Text);
                    Console_out("Decoding is ready");
                }
                else Console_out("Form is not correct");
            }
            else
            {
                Console_out("Decrypt function is automatic");
                if (form_is_correct_for_auto())
                {
                    Console_out("Form is correct");
                    _text = textBox7.Text;
                    Console_out("Decoding");
                    textBox7.Text = intTostr(textBox7.Text);
                    Console_out("Decoding is ready");
                }
                else Console_out("Form is not correct");
            }
        }


        ///////////////////////////////User Interface decor////////////////////////////////////////////////////////////////
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            radioButton2.Checked = false;
            textBox4.Enabled = false;
            textBox8.Enabled = false;
            textBox6.Enabled = false;
            button1.Enabled = false;
            dataGridView1.Columns.Add("A", "A");
            dataGridView1.Columns.Add("B", "B");
            dataGridView1.Columns.Add("A mod B", "A mod B");
            dataGridView1.Columns.Add("A div B", "A div B");
            dataGridView1.Columns.Add("x", "x");
            dataGridView1.Columns.Add("y", "y");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                radioButton2.Checked = false;
                textBox3.Enabled = false;
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox5.Enabled = true;
                button1.Enabled = false;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                radioButton1.Checked = false;
                textBox3.Enabled = true;
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox5.Enabled = false;
                button1.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox2.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox5.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox6.Text);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox8.Text);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox4.Text);
        }
        //////////////////////////////////////////////Modules//////////////////////////////////////////////////////////////////////

        private void button10_Click(object sender, EventArgs e)
        {
            if (dataGridView1[0, 0].Value != null && dataGridView1[1, 0].Value != null)
            {
                BigInteger nod, x, y, a = BigInteger.Parse(dataGridView1[0, 0].Value.ToString()), b = BigInteger.Parse(dataGridView1[1, 0].Value.ToString());
                EvklidAndFastExp.Evklid(a, b, out nod, out x, out y, true, dataGridView1);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (textBox10.Text != "" && textBox9.Text != "" && textBox11.Text != "")
            {
                BigInteger a = BigInteger.Parse(textBox9.Text), ex = BigInteger.Parse(textBox10.Text), n = BigInteger.Parse(textBox11.Text);
                textBox12.Text = EvklidAndFastExp.FastModPow(a, ex, n).ToString();
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (textBox14.Text != "")
            {
                BigInteger n = BigInteger.Parse(textBox14.Text);
                Console_out("Prime Test");
                Console_out("Testing n<2");
                if (n < 2) { label2.Text = "Составное"; label5.Text = "n<2"; }
                else
                {
                    Console_out("Testing num in 2-257");
                    if (Prime_Tests.prime_2_257(n)) { label2.Text = "Простое"; label5.Text = "Простое в пределах 2-257"; }
                    else
                    {
                        Console_out("Testing num is multiple in 2-257");
                        if (!Prime_Tests.primed(n)) { label2.Text = "Составное"; label5.Text = "Делится на простое из отрезка 2-257"; }
                        else
                        {
                            Console_out("Testing with Miller-Rabin test");
                            if (!Prime_Tests.MillerRabinTest(n)) { label2.Text = "Составное"; label5.Text = "тест Миллера-Рабина"; }
                            else { label2.Text = "Простое"; label5.Text = "тест Миллера-Рабина"; }
                        }
                    }
                }
                Console_out("Prime test is complete");
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (textBox15.Text != "")
            {
                label8.Text = EvklidAndFastExp.Bin(BigInteger.Parse(textBox15.Text)).Length.ToString();
            }
        }
    }
}