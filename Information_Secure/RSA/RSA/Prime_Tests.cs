﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace RSA
{
    class Prime_Tests
    {
        public static bool prime_2_257(BigInteger n)
        {
            return (n == 2 || n == 3 || n == 5 || n == 7 || n == 11 || n == 13 || n == 17 || n == 19 || n == 23 || n == 29 || n == 31 || n == 37 || n == 41 || n == 43 || n == 47 || n == 53 || n == 59 || n == 61 || n == 67 || n == 71
             || n == 73 || n == 79 || n == 83 || n == 89 || n == 97 || n == 101 || n == 103 || n == 107 || n == 109 || n == 113 || n == 127 || n == 131 || n == 137 || n == 139 || n == 149 || n == 151 || n == 157 || n == 163 || n == 167 || n == 173
             || n == 179 || n == 181 || n == 191 || n == 193 || n == 197 || n == 199 || n == 211 || n == 223 || n == 227 || n == 229 || n == 233 || n == 239 || n == 241 || n == 251 || n == 257);
        }
        public static bool primed(BigInteger n)
        {
            if ((n & 1) == 0) return false;
            int[] a = { 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 
                          73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 
                          179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257 };
            foreach (int i in a)  
                if (n % i == 0) 
                    return false;
            return true;
        }

        public static bool MillerRabinTest(BigInteger n)
        {
            int s = 0; BigInteger a, x, d = n - 1;
            while ((d & 1) == 0) { d >>= 1; s++; } //while(d % 2 == 0)
            a = 1;
            double logn = BigInteger.Log10(n);
            for (int i=0;i<=logn;i++)
            {
                a++;
                x = EvklidAndFastExp.FastModPow(a, d, n);
                if (x == 1 || x == n - 1) continue;
                for (int j = 0; j < s - 1; j++)
                {
                    x = (x * x) % n;
                    if (x == 1) return false;
                    else if (x == n - 1) break;
                }
                if (x != n - 1) return false;
            }
            return true;
        }

        /*public static bool MillerRabinTest(BigInteger n)
        {
            int s = 0; BigInteger a, x, d = n - 1;
            while ((d & 1) == 0) { d >>= 1; s++; } //while(d % 2 == 0)
            a = 1;
            for (BigInteger nn = 1; nn <= n; nn <<= 1)
            {
                a++;//= BigNumsGenerator.getRandom(2, n - 1);
                x = EvklidAndFastExp.FastModPow(a, d, n);
                if (x == 1 || x == n - 1) continue;
                for (int j = 0; j < s - 1; j++)
                {
                    x = (x * x) % n;
                    if (x == 1) return false;
                    else if (x == n - 1) break;
                }
                if (x != n - 1) return false;
            }
            return true;
        }*/

        public static bool Ferma_Test(BigInteger n)
        {
            BigInteger a;
            for (int i = 0; i < 100; i++)
            {
                a = BigNumsGenerator.getRandom(2, n - 1);
                if (EvklidAndFastExp.FastModPow(a, n - 1, n) != 1) return false;
            }
            return true;
        }

        public static bool number_is_prime(BigInteger n)
        {
            if (n < 2) return false;
            else if (prime_2_257(n)) return true;
            else if (!primed(n)) return false;
            //else if (!Ferma_Test(n)) return false;
            else return MillerRabinTest(n);
        }
    }
}
