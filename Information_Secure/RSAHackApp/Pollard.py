import sys
import random
import math
import time
from threading import Thread

def abs(n):
	if n<0:
		return -n
	else:
		return n

def getOutMessage(x,d,n):
    y = str(pow(x, d, n))
    mes = ""
    for i in range(0, len(y), 2):
        res = y[i: i + 2]
        ch = chr(1024 + int(res))
        mes += ch;
    return mes

def Evklid(a, b):
        buf = max(a, b);
        x,y,u,v = 0,1,1,0
        while a != 0:
            r = b % a;
            q = b//a
            m, n = x-u*q, y-v*q
            b,a,x,y,u,v = a,r,u,v,m,n
        return x + buf;

def Pollard(n):
    x = 2
    y = 1
    iterations = 0
    stage = 2
    d=1
    while True:
    	iterations+=1
    	if iterations == stage:
    		y=x
    		stage*=2
    	x = (x * x + 1) % n
    	d=(d*abs(x-y))%n
    	if iterations%1000==0:
    		d=math.gcd(n,d)
    		if d>1:
    			return d,iterations

def hackFunc(n,e,mess):
    start = time.clock()
    p,iterations=Pollard(n)
    q=n//p
    fi=(p-1)*(q-1)
    d=Evklid(e, fi)
    messOut=getOutMessage(mess,d,n)
    stop = time.clock()
    return p,q,d,fi,messOut,iterations,(stop-start)


class HackThread(Thread):

    def __init__(self, n,e,messIn,pEdit,qEdit,fiEdit,dEdit,messOutEdit,iterationsLabel,timeLabel):
        Thread.__init__(self)
        self.n=n
        self.e=e
        self.messIn=messIn
        self.pEdit=pEdit
        self.qEdit=qEdit
        self.fiEdit=fiEdit
        self.dEdit=dEdit
        self.messOutEdit=messOutEdit
        self.iterationsLabel=iterationsLabel
        self.timeLabel=timeLabel

    def run(self):
        p,q,d,fi,mess,iterations,timeWork=hackFunc(self.n,self.e,self.messIn)
        self.pEdit.text.emit(str(p))
        self.qEdit.text.emit(str(q))
        self.fiEdit.text.emit(str(fi))
        self.dEdit.text.emit(str(d))
        self.messOutEdit.text.emit(str(mess))
        self.iterationsLabel.text.emit(str(iterations)+' итераций')
        self.timeLabel.text.emit('Время работы программы: %.2f'%(timeWork/60) + " минут.")

#print(Pollard(875078015194165779807004996891)[0])