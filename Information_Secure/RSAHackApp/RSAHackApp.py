#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel, QLineEdit, QPushButton, QTextEdit, QGridLayout, QApplication)
from PyQt5.QtCore import QCoreApplication, QObject, pyqtSignal, pyqtSlot
import Pollard
import _thread

class RSAHackApp(QWidget):

    def __init__(self):
        super().__init__()
        self.nEdit=QLineEdit()
        self.eEdit = QLineEdit()
        self.dEdit = QLineEdit()
        self.pEdit = QLineEdit()
        self.qEdit = QLineEdit()
        self.fiEdit = QLineEdit()
        self.messInEdit = QLineEdit()
        self.messOutEdit = QLineEdit()
        self.iterationsLabel=QLabel('')
        self.timeLabel=QLabel('')
        self.okButton = QPushButton("Hack")
        self.dEdit.setEnabled(False)
        self.pEdit.setEnabled(False)
        self.qEdit.setEnabled(False)
        self.fiEdit.setEnabled(False)
        self.messOutEdit.setEnabled(False)
        self.th=""
        self.initUI()

    class TextForEdit(QObject):
        text = pyqtSignal(str)

    class TextForLabel(QObject):
        text=pyqtSignal(str)

    def setText_p(self, text):
        self.pEdit.setText(text)

    def setText_q(self, text):
        self.qEdit.setText(text)

    def setText_d(self, text):
        self.dEdit.setText(text)

    def setText_outMess(self, text):
        self.messOutEdit.setText(text)

    def setText_fi(self, text):
        self.fiEdit.setText(text)

    def setText_iterations(self, text):
        self.iterationsLabel.setText(text)

    def setText_time(self, text):
        self.timeLabel.setText(text)
        self.blockComponents(True)

    def blockComponents(self,block):
        self.nEdit.setEnabled(block)
        self.eEdit.setEnabled(block)
        self.messInEdit.setEnabled(block)
        self.okButton.setEnabled(block)

    @pyqtSlot()
    def startThreadF(self):
        self.timeLabel.setText('Hacking. Please wait.')
        self.blockComponents(False)
        n=int(self.nEdit.text())
        e=int(self.eEdit.text())
        mess=int(self.messInEdit.text())
        textForEdit_OutMess = RSAHackApp.TextForEdit()
        textForEdit_OutMess.text.connect(self.setText_outMess)
        textForEdit_p = RSAHackApp.TextForEdit()
        textForEdit_p.text.connect(self.setText_p)
        textForEdit_q = RSAHackApp.TextForEdit()
        textForEdit_q.text.connect(self.setText_q)
        textForEdit_fi = RSAHackApp.TextForEdit()
        textForEdit_fi.text.connect(self.setText_fi)
        textForEdit_d = RSAHackApp.TextForEdit()
        textForEdit_d.text.connect(self.setText_d)
        textForLabel_iterations = RSAHackApp.TextForEdit()
        textForLabel_iterations.text.connect(self.setText_iterations)
        textForLabel_time = RSAHackApp.TextForEdit()
        textForLabel_time.text.connect(self.setText_time)

        self.th = Pollard.HackThread(n,e,mess,textForEdit_p,textForEdit_q,textForEdit_fi,textForEdit_d,textForEdit_OutMess,textForLabel_iterations,textForLabel_time)
        self.th.start()

    def initUI(self):
        nLabel = QLabel('n')
        eLabel = QLabel('e')
        dLabel = QLabel('d')
        pLabel = QLabel('p')
        qLabel = QLabel('q')
        fiLabel = QLabel('fi')
        messInLabel = QLabel('messageIN')
        messOutLabel = QLabel('messageOut')
        self.okButton.clicked.connect(self.startThreadF)
        grid = QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(nLabel, 1, 0)
        grid.addWidget(self.nEdit, 1, 1)
        grid.addWidget(eLabel, 2, 0)
        grid.addWidget(self.eEdit, 2, 1)
        grid.addWidget(messInLabel, 3, 0)
        grid.addWidget(self.messInEdit, 3, 1)
        grid.addWidget(pLabel, 4, 0)
        grid.addWidget(self.pEdit, 4, 1)
        grid.addWidget(qLabel, 5, 0)
        grid.addWidget(self.qEdit, 5, 1)
        grid.addWidget(fiLabel, 6, 0)
        grid.addWidget(self.fiEdit, 6, 1)
        grid.addWidget(dLabel, 7, 0)
        grid.addWidget(self.dEdit, 7, 1)
        grid.addWidget(messOutLabel, 8, 0)
        grid.addWidget(self.messOutEdit, 8, 1)
        grid.addWidget(self.okButton,9,1)
        grid.addWidget(self.iterationsLabel,10,0)
        grid.addWidget(self.timeLabel,10,1)
        self.setLayout(grid)
        self.setGeometry(300, 300, 450, 300)
        self.setWindowTitle('Hack RSA')
        self.show()

    def closeEvent(self, QCloseEvent):
        self.th.join()
        super().closeEvent(QCloseEvent)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = RSAHackApp()
    sys.exit(app.exec_())