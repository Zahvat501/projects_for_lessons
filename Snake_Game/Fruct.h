
class fruct
{
public:
	int x, y;
	double _fruct_color_red, _fruct_color_green, _fruct_color_blue;

	void set_color(double fruct_color_red, double fruct_color_green, double fruct_color_blue)
	{
		_fruct_color_red = fruct_color_red;
		_fruct_color_green = fruct_color_green;
		_fruct_color_blue = fruct_color_blue;
	}

	void New(int N, int M)
	{
		x = rand() % N;
		y = rand() % M;
	}

	void Draw(int _scale)
	{
		glColor3f(_fruct_color_red, _fruct_color_green, _fruct_color_blue);
		glRectf(x*_scale, y*_scale, (x + 1)*_scale, (y + 1)*_scale);
	}
};