#include "Snake_Block.h"

class Snake
{
public:
	snake_block *s, *head; int max_len, current_len, init_len; double _snake_color_red, _snake_color_green, _snake_color_blue;
	Snake() { s = NULL; max_len = 0; current_len = 0; init_len = 0; _snake_color_red = 0; _snake_color_green = 0; _snake_color_blue = 0; }
	Snake(int _max_len, int _init_len, snake_block _init_coords, double _r, double _g, double _b)
	{
		max_len = _max_len;
		init_len = _init_len;
		current_len = _init_len;
		s = new snake_block[_max_len];
		s[0] = _init_coords;
		_snake_color_red = _r;
		_snake_color_green = _g;
		_snake_color_blue = _b;
		head = &s[0];
	}
	void Draw(int _scale)
	{
		for (int i = 0; i < current_len; i++) s[i].Draw(_scale, _snake_color_red, _snake_color_green, _snake_color_blue);
	}

	void Up() { s[0].y += 1; }

	void Down() { s[0].y -= 1; }

	void Left() { s[0].x -= 1; }

	void Right() { s[0].x += 1; }

	bool is_can_go(snake_block b)
	{
		for (int i = 1; i < current_len; i++)
			if (b == s[i])
				return false;
		return true;
	}

	bool is_can_go_down()
	{
		return is_can_go(snake_block(s[0].x, s[0].y - 1));
	}

	bool is_can_go_up()
	{
		return is_can_go(snake_block(s[0].x, s[0].y + 1));
	}

	bool is_can_go_left()
	{
		return is_can_go(snake_block(s[0].x-1, s[0].y));
	}

	bool is_can_go_right()
	{
		return is_can_go(snake_block(s[0].x+1, s[0].y));
	}
};