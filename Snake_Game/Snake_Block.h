#include "Fruct.h"

class snake_block
{
public:
	int x; int y;
	snake_block() { x = 0;y = 0; }
	snake_block(int _x, int _y) 
	{ 
		x = _x; 
		y = _y;
	}

	bool operator ==(const fruct &m_fruct)
	{
		return this->x == m_fruct.x && this->y == m_fruct.y;
	}

	bool operator ==(const snake_block &sb)
	{
		return this->x == sb.x && this->y == sb.y;
	}

	snake_block& operator =(const snake_block &sb)
	{
		this->x = sb.x; this->y = sb.y;
		return *this;
	}

	void Draw(int _scale, double _snake_color_red, double _snake_color_green, double _snake_color_blue)
	{
		glColor3f(_snake_color_red, _snake_color_green, _snake_color_blue);
		glRectf(x*_scale, y*_scale, (x + 0.9)*_scale, (y + 0.9)*_scale);
	}
};
