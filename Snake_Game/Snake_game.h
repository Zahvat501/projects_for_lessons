#include "stdafx.h"
#include <GL/glut.h> 
#include <time.h> 
#include <random> 
#include <iostream> 
#include "Snake.h"
#include <string>
using namespace std;

const int N = 30, M = 20,									  // ���������� ��������� 
		 scale = 25,										  // ������ ��������.  
		 window_size_x = scale*N, window_size_y = scale*M,   // ������ � ������ ����  
		 real_window_size_x = window_size_x, real_window_size_y = window_size_y+scale*2,
		 init_snake_len = 4, max_snake_len = 100, num_of_fructs = 5,
		 text_pos_x= 100, text_pos_y= real_window_size_y-scale,
		 speed = 100;									      // ms for timer

const double lines_color_red = 1, lines_color_green = 0, lines_color_blue = 0,
			 fruct_color_red = 0, fruct_color_green = 1, fruct_color_blue = 0,
			 snake_color_red = 0, snake_color_green = 0, snake_color_blue = 1,
			 text_color_red = 0, text_color_green = 0, text_color_blue = 1;

const snake_block init_snake_coords(10, 10);
Snake m_snake;
fruct *m;
int dir; // �����������

void Restart()
{
	for (int i = 0; i < num_of_fructs; i++) { m[i].set_color(fruct_color_red, fruct_color_green, fruct_color_blue); m[i].New(N, M); }
	m_snake = Snake(max_snake_len, init_snake_len, init_snake_coords, snake_color_red, snake_color_green, snake_color_blue);
}

void draw_string_bitmap(void *font, const char* s) 
{
	while (*s) glutBitmapCharacter(font, *s++);
}

void print_string(string s)
{
	glColor3f(text_color_red, text_color_green, text_color_blue);
	glRasterPos2f(text_pos_x, text_pos_y);
	draw_string_bitmap(GLUT_BITMAP_HELVETICA_18, s.c_str());
}

void Draw()
{
	glColor3f(lines_color_red, lines_color_green, lines_color_blue);
	glBegin(GL_LINES);
	for (int i = 0; i <= window_size_x; i += scale) { glVertex2f(i, 0); glVertex2f(i, window_size_y); }
	for (int j = 0; j <= window_size_y; j += scale) { glVertex2f(0, j); glVertex2f(window_size_x, j); }
	glEnd();
}

void tick()
{
	for (int i = m_snake.current_len; i > 0; --i) m_snake.s[i] = m_snake.s[i - 1];

	if (dir == 0)	   m_snake.Up();
	else if (dir == 1) m_snake.Left();
	else if (dir == 2) m_snake.Right();
	else if (dir == 3) m_snake.Down();

	for (int i = 0; i < num_of_fructs; i++)
		if (m_snake.s[0] == m[i])
		{
			m_snake.current_len++;
			m[i].New(N, M);
		}

	if (m_snake.s[0].x >= N || m_snake.s[0].x < 0 || m_snake.s[0].y >= M || m_snake.s[0].y < 0) Restart();
	else 
	{
		for (int i = 1; i < m_snake.current_len; i++)
			if (m_snake.s[0] == m_snake.s[i]) Restart();
	}
}

void Key(int key, int a, int b)
{
	switch (key)
	{
	case 101: if (m_snake.is_can_go_up()   ) { dir = 0; break; }
	case 102: if (m_snake.is_can_go_right()) { dir = 2; break; }
	case 100: if (m_snake.is_can_go_left() ) { dir = 1; break; }
	case 103: if (m_snake.is_can_go_down() ) { dir = 3; break; }
	}
}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	Draw();
	m_snake.Draw(scale);
	for (int i = 0; i < num_of_fructs; i++) m[i].Draw(scale);
	print_string("Length: " + to_string(m_snake.current_len));
	glFlush();
	glutSwapBuffers();
}

void timer(int = 0)
{
	Display();
	tick();
	glutTimerFunc(speed, timer, 0);
}

void Play_Game(int argc, char **argv)
{
	cout << "Snake Game by Chihirev Oleg \n";
	srand(time(0));
	m = new fruct[num_of_fructs];
	for (int i = 0; i < num_of_fructs; i++) { m[i].set_color(fruct_color_red, fruct_color_green, fruct_color_blue); m[i].New(N, M); }

	m_snake = Snake(max_snake_len, init_snake_len, init_snake_coords, snake_color_red, snake_color_green, snake_color_blue);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(real_window_size_x, real_window_size_y);
	glutCreateWindow("Snake Game by Chihirev Oleg");
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, real_window_size_x, 0, real_window_size_y);
	glutDisplayFunc(Display);
	glutSpecialFunc(Key);
	glutTimerFunc(speed, timer, 0);
	glutMainLoop();
}