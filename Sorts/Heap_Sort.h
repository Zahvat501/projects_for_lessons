
template <class T>void max_hepify(T *first, int heap_size, int number)
{
	int largest = number;
	if (2 * number + 1 < heap_size && first[2 * number + 1] > first[number]) largest = 2 * number + 1;
	if (2 * number + 2 < heap_size && first[2 * number + 2] > first[largest]) largest = 2 * number + 2;
	if (largest != number)
	{
		my_swap(first[number], first[largest]);
		max_hepify<T>(first, heap_size, largest);
	}
}

template <class T>void build_max_heap(T *first, T *last)
{
	int heap_size = last - first;
	for (int i = heap_size / 2 - 1; i >= 0; i--) max_hepify<T>(first, heap_size, i);
}

template <class T>void heap_sort(T *first, T *last)
{
	build_max_heap<T>(first, last);
	int length = last - first - 1;
	for (int i = length; i > 0; i--)
	{
		my_swap<T>(first[0], first[i]);
		max_hepify<T>(first, i, 0);
	}
}