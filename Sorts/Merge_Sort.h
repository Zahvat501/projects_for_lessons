
template <class T> class Greater
{
public:
	bool operator()(T& a, T& b) { return b > a; }
};

template <class T>T abs(T a)
{
	return a < 0 ? -a : a;
}

template <class T> class AbsLess
{
public:
	bool operator()(T& a, T& b) { return abs<T>(a) < abs<T>(b); }
};

template <class T, class Iterator, class SteakWeak>void my_merge(Iterator first, Iterator last, Iterator mid, T* buffer, SteakWeak Compare)
{
	Iterator begin1 = first, begin2 = mid;
	int i = 0;
	while (begin1 != mid && begin2 != last)
	{
		if (Compare(*begin1, *begin2)) buffer[i++] = *begin1++;
		else buffer[i++] = *begin2++;
	}
	while (begin2 != last) buffer[i++] = *begin2++;
	while (begin1 != mid) buffer[i++] = *begin1++;
	for (int j = 0; j < i; j++) *first++ = *buffer++;
}

template <class T, class Iterator, class SteakWeak>void helper(Iterator first, Iterator last, T* buffer, int size, SteakWeak Compare)
{
	if (size > 1)
	{
		Iterator mid = first + size / 2;
		helper(first, mid, buffer, size / 2, Compare);
		helper(mid, last, buffer, size - size / 2, Compare);
		my_merge(first, last, mid, buffer, Compare);
	}
}

template <class T, class Iterator, class SteakWeak>void merge_sort(Iterator first, Iterator last, SteakWeak Compare)
{
	int size = last - first;
	if (size > 1) 
	{
		T* buffer = new T[size];
		helper(first, last, buffer, size, Compare);
		delete[] buffer;
	}
}