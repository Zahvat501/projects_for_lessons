
template<class T>void Comparator(T* first, T* second)
{
	if (*first > *second)
	{
		T temp = *first;
		*first = *second;
		*second = temp;
	}
}

template<class T>void Sort_Network(T* array, int size_array)
{
	for (int MergeSz = 1; MergeSz < size_array; MergeSz *= 2)
		for (int qsz = MergeSz; qsz >= 1; qsz /= 2)
		{
			int k = 0;
			for (int j = qsz % MergeSz; j + qsz < size_array; j += 2 * qsz)
				if (j / (2 * MergeSz) == (j + qsz) / (2 * MergeSz))
					for (int i = 0; i < qsz; i++)
						if (i + j + qsz < size_array)
							Comparator<T>(array + i + j, array + i + j + qsz);
		}
}
