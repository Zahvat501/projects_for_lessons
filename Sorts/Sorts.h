#include "stdafx.h"
#include "Sort_Network.h"
#include "Merge_Sort.h"
#include "Heap_Sort.h"

template <class T>void my_swap(T &first_element, T &second_element)
{
	T temp = first_element;
	first_element = second_element;
	second_element = temp;
}

template <class T>void Bubble_Sort(T *array, int size_array)
{
	bool swaped = false;

	for (int i = 0;i < size_array;i++)
	{
		swaped = false;
		for (int j = 0;j < size_array - i - 1;j++)
		{
			if (array[j] > array[j + 1])
			{
				my_swap(array[j], array[j + 1]);
				swaped = true;
			}
		}
		if (!swaped) break;
	}
}

template <class T>void Shaker_Sort(T *array, int size_array)
{
	for (int first = 0, last = size_array - 1; first < last;)
	{
		for (int i = first; i < last; i++)
			if (array[i + 1] < array[i])
				my_swap(array[i], array[i + 1]);

		last--;

		for (int i = last; i > first; i--)
			if (array[i - 1] > array[i])
				my_swap(array[i], array[i - 1]);

		first++;
	}
}

template <class T>void Insertion_Sort(T *array, int size_array)
{
	for (int i = 1;i < size_array;i++)
	{
		T key = array[i];
		int j = i - 1;
		for (;j >= 0 && array[j] > key;j--) array[j + 1] = array[j];
		array[j + 1] = key;
	}
}

template <class T>void Gnome_Sort(T *array, int size_array)
{
	for (int i = 1, j = 2;i < size_array;)
	{
		if (array[i - 1] < array[i]) { i = j; j++; }
		else
		{
			my_swap(array[i - 1], array[i]);
			i--;
			if (i == 0) { i = j; j++; }
		}
	}
}

template <class T>void Quick_sort(T *array, int first, int last)
{
	int f = first, l = last;
	T middle = array[(f + l) / 2], temp;
	do
	{
		while (array[f] < middle) f++;
		while (array[l] > middle) l--;
		if (f <= l)
		{
			temp = array[f];
			array[f] = array[l];
			array[l] = temp;
			f++;
			l--;
		}
	} while (f < l);
	if (first < l) Quick_sort(array, first, l);
	if (f < last) Quick_sort(array, f, last);
}

template< class T >void shell_sort(T first, T last)
{
	for (int d = (last - first) / 2; d != 0; d /= 2)
		for (T i = first + d; i != last; ++i)
			for (T j = i; j - first >= d && *j < *(j - d); j -= d)
				swap(*j, *(j - d));
}